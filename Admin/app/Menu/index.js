estacaoApp.cC({
	name: 'menuCtrl',
	inject: ['$scope','$resource','ModelService'],
	init: function() {
		this.Menu = this.$resource('/api/menu/:id.json');
		this.Menu2 = this.ModelService.model.Menu;
		this.$.loading = [
			false,
			false,
			false,
			false
		];
		this.$.norecords = [
			false,
			false,
			false,
			false
		];
		this.$.ids = [
			0,
			0,
			0,
			0
		];

		this.$.menuText = 'Superior';
		this.$.textoNivel = [];
		this.$.men_tipo = 'h';
		this.q = {};
		this.tipo = 'h';
		this.sort = 'men_posicao';
		this.$.Menus = [];
		this.changeToSup();
	},
	methods: {
		_delMenus: function(level) {
			for (l=level;l<=3;l++) {
				delete(this.$.Menus[l]);
				this.$.textoNivel[l] = '';
			}
		},
		deleteMenu: function(item, level) {
			this.Menu.delete({id:item.Menu.men_id});
			this._loadMenus(level);
		},
		orderMenu: function(item, level, direction) {
			if (direction == 'down') {
				this.Menu2.save(item.Menu.men_id,
					{
						men_id: item.Menu.men_id,
						men_posicao: parseInt( item.Menu.men_posicao ) + 15
					}
				);
			} else {
				this.Menu2.save(item.Menu.men_id,
					{
						men_id: item.Menu.men_id,
						men_posicao: parseInt( item.Menu.men_posicao ) - 15
					}
				);
			}
			this._loadMenus(level);
		},
		saveOrderMenu: function(level) {
			menus = this.$.Menus[level];
			num = 1;
			menus.forEach(function(item) {
				this.Menu2.save(item.Menu.men_id,
					{
						id: item.Menu.men_id,
						men_posicao: num * 10,
					}
				);
				num++;
			}.bind(this));
			this._loadMenus(level);
		},
		changeToSup: function() {
			this.tipo = 'h';
			this.$.men_tipo = 'h';
			this.$.menuText = 'Superior';
			this._delMenus(1);
			this.loadLevelX(null, 1);
		},
		changeToInf: function() {
			this.tipo = 'v';
			this.$.men_tipo = 'v';
			this.$.menuText = 'Inferior';
			this._delMenus(1);
			this.loadLevelX(null, 1);
		},

		loadLevelX: function(item,level) {
			this._delMenus(level);
			if (item) this.$.ids[level] = item.Menu.men_id;
			if (level == 1) {
				this.q = 'Menu.men_ent_id.eq.1,Menu.men_tipo.eq.'+this.tipo+',Menu.men_parent_id.nu';
			} else {
				this.$.textoNivel[level] = '['+item.Menu.men_titulo+']';
				this.q = 'Menu.men_ent_id.eq.1,Menu.men_tipo.eq.'+this.tipo+',Menu.men_parent_id.eq.'+item.Menu.men_id;
			}
			this._loadMenus(level);
		},
		_loadMenus: function(level) {
			this.$.loading[level] = true;
			this.Menu.get(
				{
					q: this.q,
					sort: this.sort,
					populate: 'Submenu1'
				}
			).$promise
			.then(function(data){
				if (data.data.length == 0) {
					this.$.norecords[level] = true;
				} else {
					this.$.norecords[level] = false;
				}
				this.$.Menus[level] = data.data;
				this.$.loading[level] = false;
			}.bind(this));
		},
		goLevel1: function(item) {
			data = {
				men_id: item.Menu.men_id,
				men_parent_id: null
			};
			
			this.Menu.save(
				item.Menu.men_id,
				data
			).$promise
			.then(function(data){
				this.loadLevelX(false,1);
			}.bind(this));
			
		},
		goLevel2: function(item) {
			this.$.selecionaPai1Flag = true;
			this.$.selecionaPaiId = item.Menu.men_id;
		},
		selecionaPaiDest: function(item) {
			data = {
				men_id: this.$.selecionaPaiId,
				men_parent_id: item.Menu.men_id
			};
			
			this.Menu.save(
				this.$.selecionaPaiId,
				data
			).$promise
			.then(function(data){
				this.loadLevelX(false,1);
			}.bind(this));
			
		},
		goLevel2Cancel: function() {
			delete(this.$.selecionaPai1Flag);
			delete(this.$.selecionaPai1Id);
		},
		goLevel3: function(item) {
			this.$.selecionaPai2Flag = true;
			this.$.selecionaPai2Id = item.Menu.men_id;
		},
		selecionaPai2Dest: function(item) {
			console.log(item);
		},
		goLevel3Cancel: function() {
			delete(this.$.selecionaPai2Flag);
			delete(this.$.selecionaPai2Id);
		},
		
		addLevelX: function(level) {
			this.$.Form = {
				men_ent_id: 1,
				men_fixo: false,
				men_tipo: 'h',
				men_posicao: this.$.Menus[level].length,
				men_parent_id: this.$.ids[level]
			}
			this.$.addEditMenu = true;
		},
		
		addLevelXCancel: function() {
			delete(this.$.addEditMenu);
		},
		
		editLevelX: function(item, level) {
			this.Menu.get(
				{
					id: item.Menu.men_id,
					populate: 'Submenu1'
				}
			).$promise
			.then(function(data){
				this.$.Form = data.data.Menu;
			}.bind(this));
			this.$.addEditMenu = true;
		},
		
		saveLevelX: function(level) {
			this.Menu2.save(
				this.$.Form.men_id,
				this.$.Form
			);
			this._loadMenus(1);
			delete(this.$.addEditMenu);
		}
	}
});