estacaoApp.cC({
	name: 'MunicipioFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Municipio = this.ModelService.model.Municipio;
		this.Estado = this.ModelService.model.Estado;
		this.$.map = {
			center: [-15,-49],
			zoom: 4,
			options: function() {
				return {
				};
			},
			events: {
				click: function(mouse) {

				}.bind(this),
				idle: function() {

				}.bind(this)
			}
		};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_related: function() {
			this.Estado.get()
			.then(function(data){
				this.$.Estados = data.data;
			}.bind(this));
		},
		moveMarker: function(e) {
			latLng = e.latLng;
			this.$.Form.latitude = latLng.k;
			this.$.Form.longitude = latLng.D;
		},
		_add: function(){
			this.$.header = 'Novo Município';
            this.$.Form = {
            };
			this._related();
		},
		load: function() {
			this.$.loading = true;
			this.Municipio.get({id: this.$routeParams.id})
			.then(function(data){
				Municipio = data.data.Municipio;
				Municipio.latitude = parseFloat(Municipio.latitude);
				Municipio.longitude = parseFloat(Municipio.longitude);
				this.$.Form = Municipio;
				// Altera centro e zoom do mapa
				this.$.map.center = [Municipio.latitude, Municipio.longitude];
				this.$.map.zoom = 12;
				// Cria marcador
				this.$.marker = {
					position: [Municipio.latitude, Municipio.longitude],
					decimals: 5,
					options: function () {
						return {
							draggable: true
						}
					}
				};
				this.$.loading = false;
			}.bind(this));
		},
		_edit: function() {
			this.$.header = 'Editar Município';
			this._related();
			this.load();
		},
		save: function() {
			this.$.Form.latitude = this.$.marker.position[0];
			this.$.Form.longitude = this.$.marker.position[1];

			this.Municipio.save(this.$.Form).then(function(){
				this.$location.path('/municipios');
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/municipios');
		}
	}
});
