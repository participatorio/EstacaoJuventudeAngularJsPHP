estacaoApp.cC({
	name: 'OcorrenciaFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Ocorrencia = this.ModelService.model.Ocorrencia;
		this.Municipio = this.ModelService.model.Municipio;
		this.Programa = this.ModelService.model.Programa;
		this.Status = this.ModelService.model.Status;
		this.$.searchMunicipios = '';

		this.$.loadingMunicipios = false;
		this.$.loadingProgramas = false;

		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_related: function() {
			this.Status.get({
				limit: 100
			}).then(function(data){
				this.$.Status = data.data;
			}.bind(this));
		},
		getMunicipios: function() {
			if (this.$.searchMunicipios.length > 2) {
				this.$.loadingMunicipios = true;
				this.Municipio.get({
					q: 'Municipio.nome.lk.'+this.$.searchMunicipios
				}).then(function(data){
					this.$.Municipios = data.data;
					this.$.loadingMunicipios = false;
				}.bind(this));
			} else {
				delete(this.$.Municipios);
			}
		},
		getProgramas: function() {
			if (this.$.searchProgramas.length > 2) {
				this.$.loadingProgramas = true;
				this.Programa.get({
					q: 'Programa.nome_oficial.lk.'+this.$.searchProgramas
				}).then(function(data){
					this.$.Programas = data.data;
					this.$.loadingProgramas = false;
				}.bind(this));
			} else {
				delete(this.$.Programas);
			}
		},
		_add: function(){
			this.$.header = 'Nova Ocorrência';
			this._related();
		},
		_edit: function() {
			this.$.header = 'Editar Ocorrência';
			this._related();
			this.Ocorrencia.get(
				{
					id: this.$routeParams.id,
					populate: 'Programa,Municipio.Estado'
			}).then(function(data){
				this.DataService.resetTimer = true;
				this.$.Form = data.data.Ocorrencia;
				this.$.searchMunicipios = data.data.Municipio.nome;
				this.$.searchProgramas = data.data.Programa.nome_oficial;
				this.getMunicipios();
				this.getProgramas();
			}.bind(this));
		},
		save: function() {
			this.Ocorrencia.save(this.$.Form).then(function(data){
				this.$location.path('/ocorrencias');
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/ocorrencias');
		}
	}
});
