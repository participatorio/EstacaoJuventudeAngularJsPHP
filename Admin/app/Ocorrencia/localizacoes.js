estacaoApp.cC({
	name: 'OcorrenciaLocalController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Ocorrencia = this.ModelService.model.Ocorrencia;
		this.Localizacao = this.ModelService.model.Localizacao;
		this._local();
	},
	watch: {
	},
	methods: {
		_local: function(){
			this.$.header = 'Localizações';
			this.Ocorrencia.get(
				{
					id: this.$routeParams.id,
					populate: 'Localizacao'
				}
			).then(function(data){
				this.$.Ocorrencia = data.data;
			}.bind(this));
		},
		add: function() {
			this.$location.path('/ocorrencias/localizacoes/'+this.$routeParams.id+'/add');
		},
		edit: function(item) {
			this.$location.path('/ocorrencias/localizacoes/'+this.$routeParams.id+'/edit/'+item.id);
		},
		del: function(item) {
			if (!confirm('Tem Certeza?')) return false;
			this.Localizacao.del(item.id).then(function(data){
				this._local();
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/ocorrencias');
		}
	}
});
