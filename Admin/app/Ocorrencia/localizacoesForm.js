estacaoApp.cC({
	name: 'OcorrenciaLocalFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Ocorrencia = this.ModelService.model.Ocorrencia;
		this.Localizacao = this.ModelService.model.Localizacao;
		if (this.$routeParams.localizacao_id) {
			this._edit();
		} else {
			this._add();
		}
		this.$.map = {
			center: [-15,-49],
			zoom: 4,
			options: function() {
				return {

				};
			},
			events: {
				click: function(mouse) {

				}.bind(this),
				idle: function() {

				}.bind(this)
			}
		};
	},
	watch: {
	},
	methods: {
		_add: function() {
			this.$.header = 'Localizações / Nova';

			this.$.Form = {
				ocorrencia_municipal_id: this.$routeParams.id
			};
			this.$.marker = {
				position: [-15, -49], // Posição Inicial
				decimals: 5,
				options: function() {
					return { draggable: true };
				}
			};
		},
		_edit: function(item){
			this.$.header = 'Localizações / Editar';
			this.$.loading = true;
			this.Localizacao.get(
				{
					id: this.$routeParams.localizacao_id
				}
			).then(function(data){
				this.$.marker = {
					position: [parseFloat( data.data.Localizacao.latitude ), parseFloat( data.data.Localizacao.longitude )],
					decimals: 5,
					options: function() {
						return { draggable: true };
					}
				};
				this.$.Form = data.data.Localizacao;
				this.$.loading = false;
			}.bind(this));
		},
		save: function() {
			this.$.Form.latitude = this.$.marker.position[0];
			this.$.Form.longitude = this.$.marker.position[1];

			this.Localizacao.save(this.$.Form).then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/ocorrencias/localizacoes/'+this.$routeParams.id);
		}
	}
});
