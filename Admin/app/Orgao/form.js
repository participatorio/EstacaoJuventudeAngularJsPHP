estacaoApp.cC({
	name: 'OrgaoFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Orgao = this.ModelService.model.Orgao;
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_related: function() {
			params = {
				limit: 100,
				sort: 'nome'
			};

		},
		_add: function(){
			this.$.header = 'Novo Órgão Executor';
			this._related();
			this.$.Form = {};
		},
		_edit: function() {
			this.$.header = 'Editar Órgão Executor';
			this._related();
			this.$.loading = true;
			this.Orgao.get({id: this.$routeParams.id})
			.then(function(data){
				this.$.Form = data.data.Orgao;
				this.$.loading = false;
			}.bind(this))
		},
		save: function() {
			this.$.saving = true;
			this.Orgao.save(this.$.Form).then(function(){
				this.$.saving = false;
				this.$location.path('/orgaos');
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/orgaos');
		}
	}
});
