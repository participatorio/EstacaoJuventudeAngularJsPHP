estacaoApp.cC({
	name: 'OrgaoController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Orgao = this.ModelService.model.Orgao;
		// Busca
		this.$.busca = {
			nome: ''
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Orgao.params.page = newv;
				this._load();
			}
		},
		pesquisa: function() {
			this._load();
		},
		pesquisaZerar: function() {
			this.$.busca.nome = '';
			this._load();
		},
		_load: function() {
			this.$.header = 'Órgãos Executores';
			this.$.loading = true;
			this.$.norecords = false;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Orgao.get(
				{
					q: 'Orgao.nome.lk.' + this.$.busca.nome,
					page: this.Orgao.params.page,
					sort: 'nome'
				}
				).then(function (data) {
					this.$.Orgaos = data.data;
					this.$.pagination = data.pagination;
					this.$.loading = false;
					if (data.data.length == 0) {
						this.$.norecords = true;
					}
				}.bind(this)
			);
		},
		add: function() {
			this.$location.path('orgaos/add');
		},
		edit: function(item) {
			this.$location.path('orgaos/'+item.Orgao.id+'/edit');
		},
		del: function(item) {
			this.Tematica.del(item.Tematica.id).then(function(){
				this._load();
			});
		}
	}
});
