estacaoApp.cC({
	name: 'ProgramaController',
	inject: ['$scope','DataService','ModelService','growl','$location','DreamFactory'],
	data: {
		// Armazenas os registros dos Models
		data: {
		},
		// Visualização de dados do Programa
		programaView: false,
		
		// Serviço de Dados
		DataService: 'DataService',
		
		// Model principal da Controller
		ModelParams: 'ModelService.Programa',
		
		// Outros Models relacionados
		related: {
			Situacao: 'ModelService.Situacao'
		}
	},
	init: function() {
	},
	watch: {
		// Este Watcher deve estar em todos os Controller
		// Verifica se o DreamFactory montou a API e inicia a aplicação
		'{object}DataService.getAppStatus()':'_start',
		
		// Paginação
		'{object}ModelParams.page':'_load'
	},
	methods: {
		
		// Este Metodo deve estar em todos os Controllers
		// Inici a aplicação após o DreamFactory montar a API
		_start: function(newv) {
			if (newv == true) {
				this.DataService.setAppStatus(true);
				// Iniciando App
				//this._options();
				this._load(1);
			}
		},
		
		// Carrega os dados de Programas da Model
		_load: function(newPage, oldPage) {
			// Sai se API não estiver pronta
			if (!this.DataService.getAppStatus()) return false;
			// Sai se o número da página não estiver definido
			if (!newPage) return false;
			// Saiu se o novo numero de página for o mesmo do antigo
			if (newPage == oldPage) return false;
			
			// Calcula limit e offset para o novo número de página
			this.ModelService.Programa.offset = this.ModelService.Programa.limit * (newPage - 1);
			// Insere o nome da tabela nos paramestros de chamada da API
			this.ModelService.Programa.table_name = this.ModelService.tables.Programa;
			// Carrega os registros do Model
			this.DataService.flags.loading = true;
			this.ModelService.Api.getRecordsByFilter(
				this.ModelService.Programa,
				function(success) {
					this.data.Programas = success.record;
					this.data.meta = success.meta;
					this.DataService.flags.loading = false;
				}.bind(this),
				function(error) {
					alert(error);
				}
			);
		},
		
		// Opções para o Search
		_options: function() {
			
			// Situações
			this.DreamFactory.api.estacao.getRecordsByFilter(
				this.ModelService.Situacao,
				function(success){
					this.data.Situacoes = success.record;
				}.bind(this),
				function(error){
				}.bind(this)
			);
			
			// Statuses
			this.DreamFactory.api.estacao.getRecordsByFilter(
				this.ModelService.Status,
				function(success){
					this.data.Statuses = success.record;
				}.bind(this),
				function(error){
				}.bind(this)
			);
			
			// Temporalidades
			this.DreamFactory.api.estacao.getRecordsByFilter(
				this.ModelService.Temporalidade,
				function(success){
					this.data.Temporalidades = success.record;
				}.bind(this),
				function(error){
				}.bind(this)
			);
			
			// Níveis
			this.DreamFactory.api.estacao.getRecordsByFilter(
				this.ModelService.Nivel,
				function(success){
					this.data.Niveis = success.record;
				}.bind(this),
				function(error){
				}.bind(this)
			);

		},
		// Faz uma pesquisa 
		_query: function() {
			fields = [];
			if (this.Filters.nome_oficial) {
				fields.push('Programa.nome_oficial.lk.' + this.Filters.nome_oficial);
			}
			if (this.Filters.status_id) {
				fields.push('Programa.status_id.eq.' + this.Filters.status_id);
			}
			if (this.Filters.situacao_id) {
				fields.push('Programa.situacao_id.eq.' + this.Filters.situacao_id);
			}
			if (this.Filters.temporalidade_id) {
				fields.push('Programa.temporalidade_id.eq.' + this.Filters.temporalidade_id);
			}
			if (this.Filters.nivel_id) {
				fields.push('Programa.nivel_id.eq.' + this.Filters.nivel_id);
			}
			return (
				fields.toString()
			);
		},

		go: {
			add: function() {
				this.$location.path('programas/add');
			},
			edit: function(item) {
				this.$location.path('programas/'+item.id+'/edit');
			},

			tematicas: function(item) {
				this.$location.path('programas/'+item.Programa.id+'/tematicas');
			},
			orgaos: function(item) {
				this.$location.path('programas/'+item.Programa.id+'/orgaos');
			},
			aprovacoes: function(item) {
				this.$location.path('programas/'+item.Programa.id+'/aprovacoes');
			}
		},
		
		viewPrograma: function(item) {
			this.programaView = item;
		},
		viewCancel: function() {
			this.programaView = false;
		},
	}
});
