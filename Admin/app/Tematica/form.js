estacaoApp.cC({
	name: 'TematicaFormController',
	inject: ['$scope','Tematica','DataService','$location','$routeParams'],
	data: {
		form: {},
	},
	init: function() {
	},
	watch: {
		'{object}DataService.api':'_start',
	},
	methods: {
		_start: function(newv, oldv) {
			var vm = this;
			console.log(newv);
			if (newv == oldv) return false;
			if (vm.$routeParams.id) vm._edit();
			else vm._add();
		},
		_edit: function() {
			console.log('edit');
			this.Tematica.api.getRecordsByIds(
				{
					'table_name':'tematicas'
				}
			);
		},
		_add: function() {
			console.log('add');
		}
	}
});