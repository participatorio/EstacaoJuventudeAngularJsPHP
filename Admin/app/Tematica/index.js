estacaoApp.cC({
	name: 'TematicaController',
	inject: ['$scope','DataService','Tematica','growl','DreamFactory','ngDialog'],
	data: {
		// Armazenas os registros dos Models
		data: {
		},
		
		// Serviço de Dados
		DataService: 'DataService',
		
		// Model principal da Controller
		ModelParams: 'ModelService.Tematica',
		
		// Outros Models relacionados
		related: {
			TematicaPai: 'ModelService.Tematica'
		}
	},
	init: function() {
	},
	watch: {
		// Este Watcher deve estar em todos os Controllers
		// Verifica se o DreamFactory montou a API e inicia a aplicação
		'{object}DataService.getAppStatus()':'_start',
		
		// Paginação
		'{object}ModelParams.page':'_load'
	},
	methods: {
		
		// Este Metodo deve estar em todos os Controllers
		// Inici a aplicação após o DreamFactory montar a API
		_start: function(newv) {
			if (newv == true) {
				// Iniciando App
				// Carraga registros página 1
				this._load(1);
			}
		},
		
		permit: function() {
			return true;
		},
		
		// Carrega os dados de Programas da Model
		_load: function(newPage, oldPage) {
			var vm = this;
			
			// Sai se API não estiver pronta
			if (!vm.DataService.getAppStatus()) return false;
			// Sai se o número da página não estiver definido
			if (!newPage) return false;
			// Saiu se o novo numero de página for o mesmo do antigo
			if (newPage == oldPage) return false;
			
			// Carrega os registros do Model
			vm.DataService.flags.loading = true;
			
			vm.Tematica.startApi();
			
			console.log(vm.Tematica);
			vm.Tematica.api.getRecordsByFilter(
				vm.Tematica.params,
				function(success) {
					vm.data.Tematicas = success.record;
					vm.data.meta = success.meta;
					vm.DataService.flags.loading = false;
				},
				function(error) {
					//alert(error);
				}
			);
		},

		del: function(item) {
			var vm = this;
			var confirm = this.ngDialog.openConfirm({
				template: '<div class="model-body"><h4>Tem certeza?</h4></div><br><div class="model-footer clearfix"><div class="pull-right btn-group"><button class="btn btn-primary" ng-click="confirm()">Sim</button><button class="btn btn-default" ng-click="closeThisDialog()">Não</button></div></div>',
				plain: true,
			});
			confirm.then(function(msg){
				vm.ModelService.Api.getRecordsByFilter(
					vm.ModelService.Tematica,
					function(success) {
					}
				);
			}, function(msg){
			});
		}
		
	}
});
