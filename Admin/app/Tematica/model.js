estacaoApp.service('Tematica', ['DreamFactory', 'DataService', function (DreamFactory, DataService) {

	var vm = this;
	
	vm.startApi = function() {
		vm.api = DreamFactory.api.estacao;
		vm.params.table_name = 'tematicas';
	}
	
	vm.params = {
		related: '',
		order: 'nome asc',
		include_count: true,
		filter: 'parent_id is null',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
}]);
