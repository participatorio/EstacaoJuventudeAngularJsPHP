estacaoApp.cC({
	name: 'UsuarioFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		
	},
	watch: {
		
	},
	methods: {
		
		_add: function(){
			this.$.header = 'Novo Usuário';
			this._related();
		},
		_edit: function() {
			this.$.header = 'Editar Usuário';
			this._related();
			this.Usuario.get({id: this.$routeParams.id})
			.then(function(data){
				this.$.Form = data.data.Usuario;
			}.bind(this));
		},
		save: function() {
			if (this.$.Form.senha1.length > 0) {
				if (this.$.Form.senha1 != this.$.Form.senha2) {
					return false;
				}
			}
			this.Usuario.save(this.$.Form).then(function(data){
				this.$location.path('/usuarios');
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/usuarios');
		}
	}
});
