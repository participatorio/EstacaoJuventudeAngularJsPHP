estacaoApp.cC({
	name: 'UsuarioController',
	inject: ['$scope','ModelService','DataService','$location'],
	data: {
		//modelName: 'ModelService.Usuario'
	},
	init: function() {
		this.index();
	},
	watch: {
		
	},
	methods: {
		
		index: function() {
			this.ModelService.Usuario.getUsers(
				null,
				function(success) {
					this.Usuarios = success.record;
					this.meta = success.meta;
				}.bind(this),
				function(error) {
				}
			);
		},
		
		add: function() {
			this.$location.path('/usuarios/add');
		},
		
		edit: function(item) {
			this.$location.path('/usuarios/edit/'+item.id);
		},
		
		del: function(item) {
		}
	}
});