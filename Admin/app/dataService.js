estacaoApp.service('DataService', function() {
	return {
		flags: {
			loading: false,
			norecords: false
		},
		appAndUserReady: function() {
			if (this.userInfo && this.appReady) return true;
			else return false;
		},
		getAppStatus: function() {
			return this.appReady;
		},
		setAppStatus: function(val) {
			this.appReady = val;
		},
		getLoadingLevel: function() {
			return this.appLoadingLevel;
		},
		userInfo: false,
		appReady: false,
		language: 'pt_BR',
		resetTimer: false,
		appLoadingLevel: 0
	}
});
