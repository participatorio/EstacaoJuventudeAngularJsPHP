/*
Arquivos com filtros customizados


*/

// Filtro para tradução usado em i18n
// Este filtro funciona da seguinte forma:
/*
 {{'Home.index.Program'|translate}}

 dentro do diretório languagens há arquivos para cada linguagem onde
 cada termpo é separado em Folder.Página.Termo e a tradução é feita
 para a lingugem do navegador.

 Ainda não está sendo usado porque o conteúdo em banco de dados não
 está preparado para tradução.

*/

/*
estacaoApp.filter('translate', ['DataService','LanguagePtBr', function(DataService, LanguagePtBr) {
	return function(input) {
		lang = DataService.language;
		input = input.split('.');
		if (DataService.language[input[0]][input[1]][input[2]] != undefined) {
			out = DataService.language[input[0]][input[1]][input[2]];
		} else {
			out = input[2];
		}
		return out;
	};
}]);
*/
