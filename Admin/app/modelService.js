
estacaoApp.service('ModelService', ['DreamFactory', 'DataService', function (DreamFactory, DataService) {
	
	this.modelStart = function() {
		// Esta funcao precisa ser iniciada apos o Dreamfactory terminar de montar a API ( feito atualmente no AuthController
		this.Api = DreamFactory.api.estacao; // Api da Aplicação
		this.Email = DreamFactory.api.email; // Api para envio de Emails
		this.User = DreamFactory.api.user; // Api para login de usuario
		this.System = DreamFactory.api.system; // Api para controle do sistema,
		this.File = DreamFactory.api.file; // Api para Upload e Download de arquivos
	}.bind(this);
	
	
	this.tables = {
		'Programa':'programas',
		'Situacao':'situacoes',
		'Status':'status',
		'Temporalidade':'temporalidade',
		'Nivel':'niveis',
		'Tematica':'tematicas'
	};
	
	// Models Params
	this.Programa = {
		related: '',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	this.Situacao = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	this.Status = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	this.Temporalidade = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Nivel = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	// User
	this.Usuario = {
		related: 'role',
		include_count: true,
		related: '*',
		fields: '*',
		filter: '',
		order: '',
		page: 1,
		limit: 25,
		offset: 0
	};

}]);
