var estacaoApp = angular.module('estacaoApp',
	[
	'angular-growl',
	'classy',
	'ngResource',
	'ngRoute',
	'ngCookies',
	'ui.bootstrap',
	'uiGmapgoogle-maps',
	'ngSanitize',
	//'facebook',
	'ngDreamFactory'
])
	// Configura o DreamFactory
	.constant('DSP_URL', 'https://api.jsapps.com.br')
	.constant('DSP_API_KEY', 'estacao')
	.config(function(uiGmapGoogleMapApiProvider) {
		uiGmapGoogleMapApiProvider.configure({
			//    key: 'your api key',
			v: '3.20',
			libraries: 'weather,geometry,visualization',
			language: 'pt-BR'
		});
	})

	// Configura interface com Facebook
	
	//.config(function(FacebookProvider) {
	//	FacebookProvider.init('390209421167988');
	//})

	// Configurando tempo geral para mostrar as mensagens do Growl
	.config(['growlProvider', function(growlProvider) {
		growlProvider.globalTimeToLive(5000);
	}]);
