/*
* Controller Principal
*
* Controla a alteração dos temas
* Authenticação do usuário
* Temporizador do timeout da sessão
*
*/
estacaoApp.cC({
	name: 'authController',
	inject: ['$scope','$interval','$timeout','$http','ModelService','DataService','DreamFactory','growl','$cookies','$location',
				//'Facebook'
			],
	data: {
		userInfo: false,
		logging: false,
		appStatus: 'DataService.getAppStatus()',
		DataService: 'DataService',
		DFReady: 'DreamFactory.api'
	},
	watch: {
		// Iniciar apenas depois da API de DreamFactory estiver "Ready"
		'{collection}DFReady':'_startApp',
		'{object}DataService.actualPath':'_changedPath'
	},
	init: function() {
		// Sempre que a página é carregada, o DreamFactory faz consultas e precisa de alguns segundos para estar pronto.
		// Esta funçao aguada alguns milisegundos e abre uma janela modal que não pode ser fechada até que o DreamFactory API esteja pronto.
		// Isso evita que o usuário tente acessar alguma página ou função antes da hora
		
		this.$timeout(function(){
			this._initApp();
		}.bind(this),1000);
		
	},
	methods: {
		// Inicia a aplicação
		_initApp: function() {
			this.creds = {
				body: {
					email: this.$cookies.get('savedEmail'),
					password: ''
				}
			};
			// Abre janela modal fixa para carregar a API do DreamFactory
			$('#appLoading').modal(
				{
					backdrop:'static',
					keyboard: false
				}
			);
		},
		// Excutado apos o DreamFactory API estiver carregado e pronto.
		_startApp: function(newv) {
			if (newv.ready == true) {
				// DreamFactory API Carregada
				$('#appLoading').modal('hide');
				this.ModelService.modelStart();
				this.DataService.setAppStatus( true );

				if (this._checkUserCookie()) {
					this.$http.defaults.headers.common['X-DreamFactory-Session-Token'] = this.userCookie.session_id;
					this._checkUserBackend();
				} else {
					this._userContinue(false);
				}
			}
		},
		_userContinue: function(result) {
			if (result) {
				// session timer
				this.resetTimer();
				this.userInfo = this.userCookie;
				this.DataService.userInfo = this.userInfo;
			} else {
				this.$cookies.remove('userInfo');
			}
		},
		// Verifica se já existe Cookie com informações do usuário logado
		_checkUserCookie: function() {
			this.userCookie = this.$cookies.getObject('userInfo');
			if (this.userCookie) {
				return true;
			} else {
				return false;
			}
		},

		// Verifica se o usuário logado no frontend ainda es†á logado no backend
		_checkUserBackend: function() {
			this.DreamFactory.api.user.getSession(
				{
					related: '*'
				},
				function(result) {
					this._userContinue(true);
				}.bind(this),
				function(result) {
					this._userContinue(false);
				}.bind(this)
			);
		},
		// Path alterad
		_changedPath: function(newPath, oldPath) {
			this.actualPath = newPath;
		},
		// Abre tela modal de Login
		showLogin: function() {
			$('#loginForm').modal('show');
		},
		// Executa o Login no DreamFactory API
		doLogin: function() {
			loginInfo = this.creds;
			this.DreamFactory.api.user.login(this.creds,
				function(result){
					$('#loginForm').modal('hide');
					this._afterLogin(result);
				}.bind(this),
				function(result) {
					$('#loginForm').modal('show');
					this.growl.error('Login inválido! Verifique o email e a senha.');
				}.bind(this)
			);
		},
		_afterLogin: function(auth) {
			// Grava o cookie com os dados do usuário
			this.$cookies.putObject('userInfo', auth);
			// Configura o token da sessão para ser enviado em todos os requests HTTP
			this.$http.defaults.headers.common['X-DreamFactory-Session-Token'] = auth.session_id;
			// Manter dados do usuário no DataService
			this.DataService.userInfo = auth;
			// Manter dads do usuário no controller para exibição da tela
			this.userInfo = auth;
			this.growl.success('Login efetuado com sucesso!');
		},
		doLogout: function() {
			this.DreamFactory.api.user.logout();
			this.$cookies.remove('userInfo');
			this.userInfo = false;
			this.growl.success('Logout efetuado com sucesso!');
			this.creds = {
				body: {
						email: (this.$cookies.get('savedEmail'))?(this.$cookies.get('savedEmail')):'',
						password: ''
					}
			};
		},
		// reseta o timeout da sessão do usuário
		resetTimer: function() {
			//this._auth();
			this.timer = new Date();
			this.timer.setMinutes(20);
			this.timer.setSeconds(01);
			this.DataService.resetTimer = false;
		},
		// relógio (temporizador) da sessão do usuário
		// Dimuniu o contador de 1 em 1 segundo
		// Quando chegar a zero, desvia para a action de logout do servidor
		userInterval: function() {
			// Se a flag resetTimer estiver como true, reseta o contador
			if (this.DataService.resetTimer) this.resetTimer();
			// Diminui 1 segundo no relógio
			this.timer.setSeconds(this.timer.getSeconds()-1);
			// Pega os minutos do relógio
			min = this.timer.getMinutes();
			// Cria uma variavel no navagedor com os minutos atuais ( usado para mudar a cor do relógio )
			this.$.minutesToTimeout = parseInt(min);
			// Adiciona '0' a esquerda dos minutos
			if (min < 10) min = '0'+min;
			// Pega os segundos do relógio
			sec = this.timer.getSeconds();
			// Adiciona '0' a esquerda dos segundos
			if (sec < 10) sec = '0'+sec;

			// Logoff quando timeout acabar
			if (min == '00' && sec == '00') {
				location.href = '/logout';
			}
			// Cria uma variável no navagedor com o relógio
			this.sessionTimeout = min+':'+sec;
		},
		FBLogin: function() {
			this.Facebook.login(function(response) {
				this._FBgetLoginStatus();
			}.bind(this),{scope: 'email'});
		},
		FBLogout: function() {
			this.Facebook.logout(function(res){
				this._FBgetLoginStatus();
				this.$FBUser = {};
			}.bind(this));
		},
		_FBgetLoginStatus: function() {
			this.Facebook.getLoginStatus(function(response) {
				if(response.status === 'connected') {
					this.FBLoggedIn = true;
					this._FBme();
				} else {
					this.FBLoggedIn = false;
				}
			}.bind(this));
		},
		_FBme: function() {
			this.Facebook.api('/me', function(response) {
				this.FBUser = response;
				//facebook_id = response.id;
				this.ModelService.Jovem.table_name = this.ModelService.tables.Jovem;
				this.ModelService.Api.getRecordsByFilter(
					this.ModelService.Jovem,
					function(success) {
						if (success.record.length == 0) {
							this._FBSaveNewJovem();	
						} else {
							this.FBJovem = success.record;
						}
					}.bind(this)
				);
			}.bind(this));
		},
		_FBSaveNewJovem: function() {
			if(this.FBUser.gender=='male') sexo=1; else sexo=2;
			jovem = {
				nome: this.FBUser.name,
				email: this.FBUser.email,
				sexo_id: sexo,
				facebook_id: this.FBUser.id,
				link: this.FBUser.link
			};
			this.ModelService.Api.createRecords(
				{
					table_name: this.ModelService.tables.Jovem,
					body: jovem
				},
				function(success) {
					console.log(success);
				}.bind(this)
			);
		}
	}
});
