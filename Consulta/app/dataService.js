estacaoApp.service('DataService', function() {
	return {
		flags: {
			loading: false,
			norecords: false
		},
		appAndUserReady: function() {
			if (this.appReady) return true;
			else return false;
		},
		getAppStatus: function() {
			return this.appReady;
		},
		setAppStatus: function(val) {
			this.appReady = val;
		},
		userInfo: false,
		appReady: false,
		language: 'pt_BR',
		resetTimer: false
	}
});
