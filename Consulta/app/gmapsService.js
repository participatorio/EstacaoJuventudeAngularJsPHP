estacaoApp.service('GMapsService',['uiGmapIsReady', function(uiGmapIsReady) {
	
	_this = this;
	
	this.gmapLoading = false;
	
	this.data = {
		tematicas: [],
		estados: [],
		municipios: [],
		ocorrencias: []
	};
	
	this.visao = 'E';
	
	this.geoActions = {
		setVisao: function(visao) {
			this.data.ocorrencias = [];
			this.map.markers.ocorrencias = [];
			if (!visao) {
				this.visao = 'E';
				return true;
			}
			this.visao = visao;
			if (visao == 'E') $('#btnVisaoEstados').addClass('active');
			if (visao == 'M') $('#btnVisaoMunicipios').addClass('active');
			if (visao == 'A') $('#btnVisaoAcoes').addClass('active');
			if (visao == 'U') $('#btnVisaoUsuario').addClass('active');
			return true;
		}.bind(this),
		getVisao: function() {
			return this.visao;
		}.bind(this)
	};
	
	// GMap
	this.map = {
		center: {
			latitude: -15,
			longitude: -46
		},
		zoom: 4,
		options: {
			mapTypeControl: false
		},
		markers: {
			estados: [],
			municipios: [],
			ocorrencias: [],
			usuario: []
		},
		controls: {
			estados: [],
			municipios: [],
			tematicas: []
		}
	};
	
	// Camadas de Marcadores
	this.layers = {
		usuario: true,
		estados: true,
		municipios: true,
		localizacoes: true
	};
	
	// Busca no GMaps
	this.search = {
		estado_id: null,
		municipio_id: null,
		tematica_id: null,
		tematicas_ids: []
	};
	
	// Dados Geolocalizados
	this.userGeo = {};
	
	this.brasil = {
		center: {
			latitude: -15,
			longitude: -46
		},
		zoom: 4
	};
	
	// Mensagem no rodapé do GMaps
	this.status = {
		code: 0,
		state:'success',
		msg: 'Carregado.'
	};
	
}]);

estacaoApp.service("Geolocation", ['$q', '$window', function($q, $window) {
		this.getLocation = function() {
		var deferred;
		deferred = $q.defer();
		if ($window.navigator && $window.navigator.geolocation) {
			$window.navigator.geolocation.getCurrentPosition(function(position) {
				return deferred.resolve(position.coords);
			}, function(error) {
				return deferred.reject("Unable to get your location");
			});
		} else {
			deferred.reject("Your browser cannot access to your position");
		}
		return deferred.promise;
		};
		this.getAddress = function() {
			var deferred,
			_this = this;
			this.geocoder || (this.geocoder = new google.maps.Geocoder());
			deferred = $q.defer();
			this.getLocation().then(function(coords) {
				var latlng;
				latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
				return _this.geocoder.geocode({
					latLng: latlng
				}, function(results, status) {
					if (status === google.maps.GeocoderStatus.OK) {
						return deferred.resolve(_this.extractAddress(results));
					} else {
						return deferred.reject("cannot geocode status: " + status);
					}
				}, function() {
					return deferred.reject("cannot geocode");
				});
			});
			return deferred.promise;
		};
		this.extractAddress = function(addresses) {
			var address, component, result, _i, _j, _len, _len1, _ref;
			result = {};
			for (_i = 0, _len = addresses.length; _i < _len; _i++) {
				address = addresses[_i];
				result.endereco || (result.endereco = address.formatted_address);
				result.coordenadas || (result.coordenadas = [address.geometry.location.A, address.geometry.location.F]);
				_ref = address.address_components;
				for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
					component = _ref[_j];
					if (component.types[0] === "route") {
						result.rua || (result.rua = component.long_name);
					}
					if (component.types[0] === "locality") {
						result.cidade || (result.cidade = component.long_name);
					}
					if (component.types[0] === "postal_code") {
						result.cep || (result.cep = component.long_name);
					}
					if (component.types[0] === "country") {
						result.pais || (result.pais = component.long_name);
					}
					if (component.types[0] === "neighborhood ") {
						result.bairro || (result.bairro = component.long_name);
					}
					if (component.types[0] === "administrative_area_level_1") {
						result.estado || (result.estado = component.long_name);
					}
				}
			}
			return result;
		};
		return this;
	}
]);