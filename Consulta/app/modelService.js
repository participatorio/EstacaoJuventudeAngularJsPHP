
estacaoApp.service('ModelService', ['DreamFactory', 'DataService', function (DreamFactory, DataService) {
	
	this.modelStart = function() {
		// Esta funcao precisa ser iniciada apos o Dreamfactory terminar de montar a API ( feito atualmente no AuthController
		this.Api = DreamFactory.api.estacao; // Api da Aplicação
		this.Email = DreamFactory.api.email; // Api para envio de Emails
		this.User = DreamFactory.api.user; // Api para login de usuario
		this.System = DreamFactory.api.system; // Api para controle do sistema
	}.bind(this);
	
	
	this.tables = {
		'Programa':'programas',
		'Situacao':'situacoes',
		'Status':'status',
		'Temporalidade':'temporalidade',
		'Nivel':'niveis',
		'Tematica':'tematicas',
		'Ocorrencia':'ocorrencias',
		'OcorrenciaMunicipal':'ocorrencias_municipais',
		'Estado':'estados',
		'Municipio':'municipios',
		'MunicipioOcorrencia':'municipios_ocorrencias',
		'Contador':'contadores',
		'EstadoOcorrencia':'estados_ocorrencias',
		'Jovem':'jovens'
	};
	
	// Models Params
	
	this.Jovem = {
		order: '',
		include_count: true,
		filter: '',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.EstadoOcorrencia = {
		order: 'nome ASC',
		filter: '',
		fields: '*',
		page: 1,
		limit: 50,
		offset: 0
	};
	
	this.Contador = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Ocorrencia = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.OcorrenciaMunicipal = {
		related: 'localizacoes_by_ocorrencia_municipal_id,municipios_by_municipio_id,programas_by_programa_id',
		order: '',
		include_count: true,
		filter: 'status_id = 1 and situacao_id = 1',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Estado = {
		related: 'municipioses_by_estado_id',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Municipio = {
		order: '',
		include_count: true,
		filter: '',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.MunicipioOcorrencia = {
		order: '',
		include_count: true,
		filter: '',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Programa = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Situacao = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	this.Status = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	this.Temporalidade = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Nivel = {
		related: '*',
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	this.Tematica = {
		order: '',
		include_count: true,
		filter: '',
		fields: '*',
		page: 1,
		limit: 25,
		offset: 0
	};
	
	// User
	this.Usuario = {
		related: 'role',
		include_count: true,
		related: '*',
		fields: '*',
		filter: '',
		order: '',
		page: 1,
		limit: 25,
		offset: 0
	};

}]);
