estacaoApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeController',
		controllerAs: 'homeCtrl',
		templateUrl: '/app/Home/index.html'
	})
	.when('/pesquisa', {
		controller: 'pesquisaController',
		controllerAs: 'pesquisaCtrl',
		templateUrl: '/app/Pesquisa/index.html'
	})
	.when('/programas', {
		controller: 'programaController',
		controllerAs: 'programaCtrl',
		templateUrl: '/app/Programa/index.html'
	})
	.otherwise({
		templateUrl: '/app/errors/404.html'
	});
	
});